package example;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import com.google.gson.*;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

// Clase que cifra y descifra el archivo json con cifrado AES
public class CifradoJson {
	
	private String clave;
	
	static Cipher cipher;
	public static final String ENCRYPTION_SCHEME = "AES";
	
	
	// Crea un objeto con la clave
	public CifradoJson (String clave) {
		this.clave = clave;
	}
		
	// ----- Métodos públicos -----
	
	// Cifrar archivo
	public String cifrarArchivo(String archivo) throws Exception {
		String cadenaSimple = jsonACadena(archivo);
		return cifrar(cadenaSimple);
	}
	
	// Cifrar texto
	public String cifrarTexto(String texto) throws Exception {
		return texto.trim().replaceAll("\n", "");
	}
	
	// encriptar cadena con un algoritmo de cifrado simétrico con clave
		public String cifrar(String texto) throws Exception {
			
			// obtenemos la SecretKey desde la clave que tenemos
			SecretKey key = secretKeyDesdeClave(clave);
			
		    // encriptar el texto
			byte[] plainTextByte = texto.getBytes();
			cipher = Cipher.getInstance(ENCRYPTION_SCHEME);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encryptedByte = cipher.doFinal(plainTextByte);
			Base64.Encoder encoder = Base64.getEncoder();
			String encryptedText = encoder.encodeToString(encryptedByte);
			//System.out.println("EncryptedText = " + encryptedText);
			System.out.println("Cifrado generado con éxito");
			return encryptedText;
			}
	
	
	// Genera una cadena de texto con el descifrado
	public String generarTexto(String cifrado) throws Exception {
		return descifrar(cifrado);
	}
	
	// Genera un archivo Json descifrado
	public void generarArchivo(String cifrado, String nombre) throws Exception {
		String textoJson = descifrar(cifrado);
		System.out.println("Descifrado realizado con éxito");
		cadenaAJson(textoJson, nombre);
	}
	
	public String getClave() {
		return this.clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	
	
	// ----- Métodos privados -----
	
	// Convierte el archivo Json en una cadena lineal que podemos cifrar
	private String jsonACadena (String path) throws IOException {
		String contenido = leerArchivo(path, Charset.defaultCharset());
		// Quitamos los espacios y las líneas
		return contenido.trim().replaceAll("\n", "");
	}
	
	// devuelve la cadena a un archivo Json con el espaciado correcto
	private void cadenaAJson (String cadena, String nombre) throws IOException {
		
		JsonParser parser = new JsonParser();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		JsonElement el = parser.parse(cadena);
		cadena = gson.toJson(el); // para devolver el formato a la cadena
		
		
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {

			fw = new FileWriter(nombre + ".json");
			bw = new BufferedWriter(fw);
			bw.write(cadena);

			System.out.println("Archivo generado");

		} catch (IOException e) {
			System.out.println("No se ha podido generar el archivo");
			e.printStackTrace();

		} finally {

			if (bw != null)
				bw.close();
			
			if (fw != null)
				fw.close();
			}
		}
	
	
	// desencriptar cadena con un algoritmo de cifrado simétrico con clave
	public String descifrar(String cifrado) throws Exception {
		
		// obtenemos la SecretKey desde la clave que tenemos
		SecretKey key = secretKeyDesdeClave(clave);
		
		//desencriptar la cadena
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] encryptedTextByte = decoder.decode(cifrado);
		cipher = Cipher.getInstance(ENCRYPTION_SCHEME);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		return decryptedText;
}

	private SecretKey secretKeyDesdeClave (String clave) {
		// el método requiere convertirlo primero a bytes
		byte[] rawkey = new byte[(int) clave.length()];
		// conviertir a secretKey  
	       return new SecretKeySpec(rawkey, ENCRYPTION_SCHEME);
	}
	
	
	
	// crea una cadena de texto a partir de un archivo
	private String leerArchivo(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
		}
	
}
