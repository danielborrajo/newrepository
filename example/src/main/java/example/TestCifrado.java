package example;

import java.io.IOException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;

import example.CifradoJson;

// clase para testear la clase CifradoJson
public class TestCifrado {
	
	static Cipher cipher;
	private static CifradoJson cjs;
	
	// variables del cifrado
	byte [] arrayBytes;
	String myEncryptionScheme;
	KeySpec ks;
	KeyGenerator keyGen;
	SecretKeyFactory skf;
	SecretKey key;
	
	public static void main(String[] args) {
		
		String clave = "ThisIsSpartaThisIsSparta";		// clave de 192 bits (24 bytes)
		String path = "/home/adminquark/workspace/example/src/main/resources/test.json";
		

		cjs = new CifradoJson(clave);

		
		// Ahora hay que generar el cifrado
		String cifrado = null;
		try {
			cifrado = cjs.cifrarArchivo(path);
		} catch (Exception e1) {
			System.out.println("No se ha podido generar el cifrado");
			e1.printStackTrace();
			return;
		}
		
		// Ahora se genera el nuevo archivo
		
		try {
			cjs.generarArchivo(cifrado, "/home/adminquark/workspace/nuevoJson");
		} catch (Exception e) {
			System.out.println("No se ha podido descifrar el texto");
			e.printStackTrace();
		}
		

	}	
}
